export class SearchUsrCriteria {
    constructor(
        public keyword: string,
        public sortBy: string
    ){

    }
}
